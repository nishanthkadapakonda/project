import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import NavBar from './components/NavBar/NavBar';
import Header from './components/Header/Header';
import { Switch, Route } from 'react-router-dom';
import DataAnalysis from './components/DataAnalysis/DataAnalysis';
import Company from './components/Company/Company';

class App extends Component {
  render() {
    return (
      <div>
        <NavBar/>
        <Header/>
        <Switch>
          <Route path='/data-analysis' component={DataAnalysis}/>
          <Route path='/company' component={Company}/>
        </Switch>
      </div>
    );
  }
}

export default App;
