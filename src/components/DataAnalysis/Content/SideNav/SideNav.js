import React from 'react';
import './SideNav.css';
const SideNav = () => {
    return(
        <div className='SideNav'>
					<ul>
						<li className = 'SideNav-li-1'>Feed Vitamins</li>
						<li className='Active-li'>Market Size</li>
						<li>Dynamics</li>
						<li>Companies</li>
						<li>News</li>
					</ul>
        </div>
    );
}
export default SideNav;