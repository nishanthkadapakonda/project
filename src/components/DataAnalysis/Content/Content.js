import React from 'react';
import './Content.css';
import SideNav from './SideNav/SideNav';
import Overview from './Overview/Overview';
const Content = (props) => {
    return(
        <div className='row'>
            <div className='col-md-3'>
               <SideNav/>
            </div>
            <div className='col-md-9'>
               <Overview/>
            </div>
        </div>
    );
}
export default Content;