import React, {Component} from 'react';
import './Overview.css';
import Chart from "react-google-charts";
class Overview extends Component {
    render() {
				const data = [
					["Element", "Density", { role: "style" }],
					["Copper", 8.94, "#b87333"], // RGB value
					["Silver", 10.49, "silver"], // English color name
					["Gold", 19.3, "gold"],
					["Platinum", 21.45, "color: #e5e4e2"] // CSS-style declaration
				];
				const options = {
					pieHole: 0.4,
					legend: "none"
				};
				const DoughnutOptions = {
					title: "",
					pieHole: 0.6,
					slices: [
					  {
						color: "#244350"
					  },
					  {
						color: "#c6dec8"
					  },
					  {
						color: "#73c3c7"
					  },
					  {
						color: "#f9fafb"
					  }
					],
					legend: 'none',
					tooltip: {
					  showColorCode: true
					},
					chartArea: {
					  left: 0,
					  top: 0,
					  width: "100%",
					  height: "80%"
					},
					fontName: "Roboto"
					};
					const pieOptions = {
						title: "",
						slices: [
							{
							color: "#244350"
							},
							{
							color: "#c6dec8"
							},
							{
							color: "#73c3c7"
							},
							{
							color: "#f9fafb"
							}
						],
						legend: 'none',
						tooltip: {
							showColorCode: true
						},
						chartArea: {
							left: 0,
							top: 0,
							width: "100%",
							height: "80%"
						},
						fontName: "Roboto"
						};
        return(
					<div className='DataAnalysis Overview'>
						<div className='OverviewHeader'>Overview</div>
						<div className='OverviewContent'>
							<div className='row'>
								<div className='col-md-6'>
									<p className='DataanalysisHeading'>Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. </p>
								</div>
								<div className='col-md-6'>
								  <p>Fig 5: Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>
									<Chart
										chartType="ColumnChart"
										width="100%"
										height="400px"
										options={options}
										data={data}
									/>
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.Lorem ipsum dolor sit amet, consectetur adipiscing elit.Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>
								</div>
							</div>
							<div className='row'>
								<div className='col-md-6'>
									<p>Fig 6: Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>
									<Chart
										chartType="PieChart"
										data={data}
										graph_id="PieChart"
										width={"100%"}
										height={"400px"}
										options={pieOptions}
									/>
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.Lorem ipsum dolor sit amet, consectetur adipiscing elit.Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>
								</div>
								<div className='col-md-6'>
									<p>Fig 7: Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>
									<Chart
										chartType="PieChart"
										width="100%"
										height="400px"
										data={data}
										options={DoughnutOptions}
									/>
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.Lorem ipsum dolor sit amet, consectetur adipiscing elit.Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>
								</div>
							</div>
						</div>
					</div>
        );
		}
	}
export default Overview;