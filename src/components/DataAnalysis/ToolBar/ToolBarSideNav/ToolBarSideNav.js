import React, {Component} from 'react';
import './ToolBarSideNav.css';
class ToolBarSideNav extends  Component {

  state = {
		modalShow: true
  }
	sideNavOpenHandler = () => {
		this.setState({modalShow: true})
	}
		sideNavOpenHandler = () => {
				this.setState({modalShow: false})
		}
    render() {
				let isShow = this.state.modalShow ? 
					<div className="container demo">
						<div className="text-center">
							<button type="button" className="btn btn-primary" data-toggle="modal" data-target="#myModal">
								Filters
							</button>
						</div>
						<div className="modal left fade" id="myModal"  role="dialog" aria-labelledby="myModalLabel">
							<div className="modal-dialog" role="document">
								<div className="modal-content">
									<div className="modal-header">
										<button type="button" className="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
										<h4 className="modal-title" id="myModalLabel"></h4>
									</div>
									<div className="modal-body">
									<div className='row'>
										<div className='DataAnalysisFilterSide col-md-9'>	
											<ul>
												<li>
													<span className='ToolBarHeading'>Additive Type</span>
													<select>
														<option>Vitamins</option>
													</select>
												</li>
												<li>
													<input type='radio' name='filter'/>
													<span className='ToolBarHeading'>Additive Type</span>
													<select className='ToolBarSelect'>
														<option>All</option>
													</select>
												</li>
												<li>
													<input type='radio' name='filter'/>
													<span className='ToolBarHeading'>AnimalType</span>
													<select className='ToolBarSelect'>
														<option>All</option>
													</select>
												</li>
												<li>
													<input type='radio' name='filter'/>
													<span className='ToolBarHeading'>Country</span>
													<select className='ToolBarSelect'>
														<option>All</option>
													</select>
												</li>
											</ul>
										</div>
									</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				 : null
        return(
					<div>
					{isShow}

				 </div>
        );
    }
}
export default ToolBarSideNav;