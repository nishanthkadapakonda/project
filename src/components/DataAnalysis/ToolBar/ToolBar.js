import React from 'react';
import './ToolBar.css';
import filter from '../../../filter.svg';
import SideToolBar from './ToolBarSideNav/ToolBarSideNav';
const ToolBar = () => {
    return(
			<div>
				<div className='MobileDisplay'>
				 <SideToolBar/>
				</div>
				<div className='ToolBar'>
					<div className='row'>
						<div className='DataAnalysisFilter col-md-3'>
							<span className='ToolBarHeading'>Additive Type</span>
							<select>
								<option>Vitamins</option>
							</select>
						</div>
						{/* <img src={filter}/> */}
						<div className='DataAnalysisFilter col-md-9'>	
							<ul>
								<li>
									<input type='radio' name='filter'/>
									<span className='ToolBarHeading'>Additive Type</span>
									<select className='ToolBarSelect'>
										<option>All</option>
									</select>
								</li>
								<li>
									<input type='radio' name='filter'/>
									<span className='ToolBarHeading'>AnimalType</span>
									<select className='ToolBarSelect'>
										<option>All</option>
									</select>
								</li>
								<li>
									<input type='radio' name='filter'/>
									<span className='ToolBarHeading'>Country</span>
									<select className='ToolBarSelect'>
										<option>All</option>
									</select>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
    );
}
export default ToolBar;