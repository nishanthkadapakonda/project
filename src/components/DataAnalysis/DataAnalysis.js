import React from 'react';
import './DataAnalysis.css';
import ToolBar from './ToolBar/ToolBar';
import Content from './Content/Content';
import SideToolBar from './ToolBar/ToolBarSideNav/ToolBarSideNav';
const DataAnalysis = (props) => {
    return(
			  <div>
					<div className='DataAnalysisToolBar'>
						<div className='container'>
							<ToolBar/>
						</div>
					</div>
					<div className=''>
						<div className='container'>
							<Content/>
						</div>
					</div>
				</div>
    );
}
export default DataAnalysis;