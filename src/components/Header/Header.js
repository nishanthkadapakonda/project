import React from 'react';
import './Header.css';
import eye from '../../eye (2).png';
import notification from '../../notification.svg';
import user from '../../user.svg';
import SubHeader from './SubHeader/SubHeader';
import Search from '../../search.svg';
const Header = (props) => {
    return(
			  <div>
					<div className="Header">
						<div className = 'container'>
							<div className="row">
								<div className="col-12 col-md-4 IntelligenceCenterLogo">
										<img src={eye} width='30px' height='20px'/><b> Intelligence Center</b>
								</div>
								<div className="col-7 col-md-4">
										<div className="Search">
											<span><img src={Search} alt="" width='30px' height='20px'/></span>
											<input type='text' placeholder=' Search the database...'/>
										</div>
								</div>
								<div className="col-5 col-md-4 float-right Notification">
									<img src={user} alt='notification' className='float-right'/>
									<img src={notification} alt='notification' className='float-right'/>
								</div>
							</div>
						</div>
					</div>
				<SubHeader/>
				</div>

    );
}
export default Header;