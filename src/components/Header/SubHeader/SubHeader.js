import React from 'react';
import {NavLink} from 'react-router-dom';
import './SubHeader.css';
const SubHeader = () => {
    return(
        <nav className="navbar navbar-expand-lg navbar-light SubHeader">
					<div className="container">
					  <div className='row'>
							<div className='col-2'>
								<button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent1" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
									<span className="navbar-toggler-icon"></span>
								</button>
							</div>
					</div>

						<div className="collapse navbar-collapse" id="navbarSupportedContent1">
							<ul className="navbar-nav mr-auto">
								<li className="nav-item">
								  <NavLink to="/explore" className="nav-link" activeClassName="subheader-active">Explore</NavLink>
								</li>
								<li className="nav-item">
									<NavLink to="/data-analysis" className="nav-link" activeClassName="subheader-active">Data  Analysis</NavLink>
								</li>
								<li className="nav-item">
									<NavLink to="/company" className="nav-link" activeClassName="subheader-active">Company</NavLink>
								</li>
								<li>
									<NavLink to="/regulations" className="nav-link" activeClassName="subheader-active">Regulations</NavLink>
								</li>
								<li>
									<NavLink to="/animal-head-count" className="nav-link" activeClassName="subheader-active">Animal Head Count</NavLink>
								</li>
								<li>
									<NavLink to="/market-analysis" className="nav-link" activeClassName="subheader-active">Parent Market Analysis</NavLink>
								</li>
								<li>
									<NavLink to="/news" className="nav-link" activeClassName="subheader-active">News</NavLink>
								</li>
								<li>
									<NavLink to="/appendix" className="nav-link" activeClassName="subheader-active">Appendix</NavLink>
								</li>
							</ul>
						</div>
					</div>
				</nav>
    );
}
export default SubHeader;