import React, {Component} from 'react';
import './NavBar.css';
import logo from '../../Mordor_Logo_new (2).png';
class NavBar extends Component {
    render() {
        return(
            <nav className="navbar navbar-expand-lg navbar-light NavBar">
						  <div className="container">
								<a className="navbar-brand" href="#"><img src= {logo} /></a>
								<button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
									<span className="navbar-toggler-icon"></span>
								</button>

								<div className="collapse navbar-collapse" id="navbarSupportedContent">
									<ul className="navbar-nav mr-auto">
										<li className="nav-item active">
											<a className="nav-link NavBar-InterStateMono" href="#">Industry Reports <span className="sr-only">(current)</span></a>
										</li>
										<li className="nav-item">
											<a className="nav-link NavBar-InterStateMono" href="#">Consulting</a>
										</li>
										<li className="nav-item">
											<a className="nav-link NavBar-InterstateMonoLgt" href="#">Intelligence Center</a>
										</li>
										<li className="nav-item dropdown">
											<a className="nav-link dropdown-toggle NavBar-DropDown" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
												$ USD
											</a>
											<div className="dropdown-menu" aria-labelledby="navbarDropdown">
												<a className="dropdown-item" href="#">Action</a>
												<a className="dropdown-item" href="#">Another action</a>
												<div className="dropdown-divider"></div>
												<a className="dropdown-item" href="#">Something else here</a>
											</div>
										</li>
									</ul>
									{/* <span className="navbar-toggler-icon headerHamburger"></span> */}
								</div>
							</div>
						</nav>
        );
    }
}
export default NavBar