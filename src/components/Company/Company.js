import React from 'react';
import './Company.css';
import ToolBar from './ToolBar/ToolBar';
import Content from './Content/Content';
const Company = (props) => {
    return(
			  <div>
					<div className='DataAnalysisToolBar'>
						<div className='container'>
							<ToolBar/>
						</div>
					</div>
					<div className=''>
						<div className='container'>
							<Content/>
						</div>
					</div>
				</div>
    );
}
export default Company;