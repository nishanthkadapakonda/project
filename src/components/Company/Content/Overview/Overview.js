import React, {Component} from 'react';
import './Overview.css';
import Chart from "react-google-charts";
import logo from '../../../../Mordor_Logo_new (2).png';
class Overview extends Component {
    render() {
        return(
					<div className='CompanyOverview'>
						<div className='CompanyOverviewHeader'>Overview</div>
						<div className='CompanyOverviewContent'>
							<div className='row'>
							  <div className='CompanyHeaderImage'>
									<div className='col-md-4'>
										<img src= {logo} className='CompanyImage'/>
									</div>
								</div>
							</div>
							<div className='row'>
								<div className='col-md-4'>
									<ul>
										<li>Lorem ipsum: Lorem</li>
										<li>Lorem ipsum: 1974</li>
										<li>Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum,Lorem ipsum.</li>
										<li>www.loremipsum.com</li>
									</ul>
								</div>
								<div className='col-md-8'>
									<ul>
										<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</li>
										<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</li>
										<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
											Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
										</li>
										<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</li>
										<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
        );
		}
	}
export default Overview;