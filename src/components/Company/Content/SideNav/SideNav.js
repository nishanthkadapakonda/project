import React from 'react';
import './SideNav.css';
const SideNav = () => {
    return(
        <div className='SideNav'>
					<ul>
						<li className = 'SideNav-li-1'>Novus</li>
						<li className='Active-li'>Overview</li>
						<li>SWOT Analysis</li>
						<li>Strategies</li>
						<li>Products</li>
						<li>Competitors</li>
					</ul>
        </div>
    );
}
export default SideNav;