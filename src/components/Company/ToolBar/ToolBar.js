import React from 'react';
import filter from '../../../filter.svg';
const ToolBar = () => {
    return(
				<div className='ToolBar'>
					<div className='row'>
						<div className='col-md-3'>
							<span className='ToolBarHeading'>Company</span>
							<select>
								<option>Novus</option>
							</select>
						</div>
					</div>
				</div>
    );
}
export default ToolBar;